/*
 * Copyright (c) 2020. Cyril Kadomsky.
 */

package salesdept_jdbc_servlet.model;

public class Product {
    private long id =-1;
    private String description = "";
    private float price = 0;


    public Product(long id, String description, float price) {
        this.id = id;
        this.description = description;
        this.price = price;
    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", price=" + price +
                '}';
    }
}
