/*
 * Copyright (c) 2020. Cyril Kadomsky.
 */

package salesdept_jdbc_servlet.ui;

import jdbc.DatabaseUtil;
import salesdept_jdbc_servlet.dao.OrderDao;
import salesdept_jdbc_servlet.model.Order;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class Demo {
    public static void main(String[] args) throws ClassNotFoundException {
        try (Connection connection = DatabaseUtil.getMysqlConnection("salesdept_jdbc_servlet") ) {
            OrderDao orderDao = new OrderDao(connection);
            List<Order> orders = orderDao.getAsList();
            for (Order order : orders) {
                System.out.println("Order from "+ order.getDate() +
                        " by " + order.getCustomer().getName() +
                        " includes: " + order.getProduct().getDescription());
            }
        } catch (SQLException e ) {
            e.printStackTrace();
        }
    }
}
