package salesdept_jdbc_servlet.controller;

import JSS.w02_pract.DatabaseUtil;
import salesdept_jdbc_servlet.dao.OrderDao;
import salesdept_jdbc_servlet.model.Order;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.util.List;

/**
 * GET /salesdept/order?filterDate=2020-11-10    -   List all orders with filter
 * GET /salesdept/order/123                 - One order by id
 * POST /salesdept/order/add?date=2020-11-10&customer=1...    - add new Order, returns URL:  /salesdept/order/123
 * POST /salesdept/order/delete?id=123
 */
@WebServlet(name = "OrderServlet", urlPatterns = "/salesdept_jdbc_servlet/order/*")
public class OrderServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // 1. Get DB connection
            // TODO: Connection conn = ConnectionService.getConnection();
            Connection conn = DatabaseUtil.getMysqlConnection("salesdept_jdbc_servlet");

            // 2. Det DAO instances (create)
            OrderDao orderDao = new OrderDao(conn);

            // 3. Parse request parameters and call DAO method



        } catch (Exception e) {  // SQLException -> ServletException
            // e.printStackTrace();
            throw new ServletException(e);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            // 1. Get DB connection
            // TODO: Connection conn = ConnectionService.getConnection();
            Connection conn = DatabaseUtil.getMysqlConnection("salesdept_jdbc_servlet");

            // 2. Det DAO instances (create)
            OrderDao orderDao = new OrderDao(conn);

            // 3. Parse request parameters and call DAO method
            String pathInfo = request.getPathInfo();
            if (pathInfo==null || pathInfo.equals("")) {
                // Option 1: no path params
                String filterDate = request.getParameter("filterDate");
                // todo: check for null, '';  add parameters to dao methods
                List<Order> orders = orderDao.getAsList();
                // 4. Pass objects to JSP VIEW
                request.setAttribute("orders",orders);
                getServletContext().getRequestDispatcher("/salesdept_jdbc_servlet/OrderView.jsp").forward(request,response);
            } else {
                // Option 2: path params: id
                long id = Long.parseLong(pathInfo);
                Order order = orderDao.getById(id);
                // 4. Pass objects to JSP VIEW
                request.setAttribute("orders",order);
                getServletContext().getRequestDispatcher("/salesdept_jdbc_servlet/OrderDetails.jsp").forward(request,response);
                // todo: add OrderDetails.jsp
            }
        } catch (Exception e) {  // SQLException -> ServletException
            // e.printStackTrace();
            throw new ServletException(e);
        }

    }
}
