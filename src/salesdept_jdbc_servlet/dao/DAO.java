/*
 * Copyright (c) 2020. Cyril Kadomsky.
 */

package salesdept_jdbc_servlet.dao;

import java.util.List;

public interface DAO<E> {   // Generic interface

    E getById(long id);
    List<E> getAsList();   // Todo: add filter or pagination

    long save(E c);
    boolean update(E c);
    boolean delete(E c);
    boolean delete(long id);

}
