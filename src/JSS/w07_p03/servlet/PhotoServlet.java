package JSS.w07_p03.servlet;

import JSS.w07_p03.bean.PhotoDAO;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * @author Cyril Kadomsky
 */
@WebServlet(name = "PhotoServlet", urlPatterns = {"/photo"})
public class PhotoServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("image/jpeg");
        int id = Integer.parseInt(request.getParameter("id"));

        OutputStream binaryOut = response.getOutputStream();

//        PhotoDAO dao = PhotoDAO.getInstance(request.getSession(true));
//
//        if (dao!=null && id<dao.getSize()) {
//            byte data[] = dao.getPhotoData(id);
//            binaryOut.write(data,0,data.length);
//        } else {
//            throw new RuntimeException("Cannot load image");
//        }
        BufferedImage img = ImageIO.read( getServletContext().getResourceAsStream("/resources/User2.png") );
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            ImageIO.write(img,"jpg", bos);
            byte[] imgData = bos.toByteArray();
            binaryOut.write(imgData,0,imgData.length);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
