package JSS.w07_p03.bean;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Cyril Kadomsky
 * stored in session scope
 */
public class PhotoDAO {

    public static final String ATTRIBUTE_NAME = "PhotoDAO";

    private List<String>  photoNames = new ArrayList<>(10);
    private List<byte[]>  photoData = new ArrayList<>(10);

    public PhotoDAO(ServletContext servletContext) {
        try {
            addPhoto("first",
                    ImageIO.read( servletContext.getResourceAsStream("/resources/User.png") ));
            addPhoto("second",
                    ImageIO.read( servletContext.getResourceAsStream("/resources/User2.png") ));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void addPhoto(String name, byte[] data) {
        photoNames.add(name);
        photoData.add(data);
    }

    public void addPhoto(String name, BufferedImage img) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            ImageIO.write(img,"jpg", bos);
            addPhoto(name, bos.toByteArray());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public List<String> getNamesAsList() {
        return photoNames;
    }

    public byte[] getPhotoData(int index) {
        return photoData.get(index);
    }

    public String getPhotoName(int index) {
        return photoNames.get(index);
    }

    public int getSize() {
        return photoData.size();
    }

    // removePhoto();

    public static PhotoDAO getInstance(HttpSession session) {
        PhotoDAO instance = (PhotoDAO) session.getAttribute(ATTRIBUTE_NAME);
        if (instance==null) {
            instance = new PhotoDAO(session.getServletContext());
            session.setAttribute(ATTRIBUTE_NAME, instance);
        }
        return instance;
    }

}
