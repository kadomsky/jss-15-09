package JSS.w07_p03.tag;

import JSS.w07_p03.bean.PhotoDAO;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

/**
 * @author Cyril Kadomsky
 */
public class PhotoDAOTag extends SimpleTagSupport {
    private String var = "photoDAO";

    public void setVar(String var) {
        this.var = var;
    }

    @Override
    public void doTag() throws JspException, IOException {
        PageContext pc = (PageContext) getJspContext();
        PhotoDAO photoDAO = PhotoDAO.getInstance( pc.getSession() );
        getJspContext().setAttribute(var, photoDAO, PageContext.PAGE_SCOPE);
    }
}
