package JSS.w02;

import java.sql.*;

public class JdbcDemo01 {
    // JDBC driver name and database URL
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost/Personnel";
    // Database credentials
    static final String USER = "root";
    static final String PASS = "1234";

    private static Connection conn = null;


    public static void main(String[] args) {
        Statement stmt = null;
        try {
            // 0. Register JDBC driver
            //Class.forName(JDBC_DRIVER);  // Not needed for SE app
            // 1. Open a connection
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            // 2. Execute a query
            System.out.println("Creating statement...");
                stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery(
                        "SELECT Employees.id as id, Employees.name as ename, Positions.name as pname, salary, age  " +
                                "FROM Employees join Positions on fkPosition=Positions.id " +
                                "WHERE Employees.name like 'A%' order by id");
                // 3. Extract data from result set
                while (rs.next()) {
                    int id = rs.getInt("id");
                    int age = rs.getInt("age");
                    String first = rs.getString("ename");
                    String position = rs.getString("pname");
                    float salary = rs.getFloat("salary");
                    System.out.println("ID: " + id + ", First: " + first + ", Age: " + age + ", Salary: " + salary + ", Position: " + position);
                }
                rs.close();


                stmt.close();
                conn.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Goodbye!");
    }

}
