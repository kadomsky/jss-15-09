package JSS.w02;

import java.sql.*;

public class JdbcEmployees {
    // JDBC driver name and database URL
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost/Personnel";
    // Database credentials
    static final String USER = "root";
    static final String PASS = "1234";

    private static Connection conn = null;

    public static void main(String[] args) {
        try {
            // 1. Open a connection
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            // 2. Execute queries


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }

    static void createTable() throws SQLException {
        System.out.println("Creating CREATE TABLE statement...");
        Statement stmt = null;
        try {
            stmt = conn.createStatement();
            DatabaseMetaData metadata = conn.getMetaData();
            ResultSet rsTables = metadata.getTables(null, null, "employees", null);
            if( !rsTables.next() ) {
                stmt.executeUpdate("CREATE TABLE employees (" +
                        "  id INT UNSIGNED NOT NULL AUTO_INCREMENT," +
                        "  name VARCHAR(45) NOT NULL," +
                        "  age INT NOT NULL," +
                        "  salary FLOAT NOT NULL," +
                        "  PRIMARY KEY (id))" +
                        "ENGINE = InnoDB;");
                stmt.executeUpdate("INSERT INTO Employees (name, age, salary) VALUES " +
                        "('Ann', 22, 2356.50), " +
                        "('Barbara', 33, 3345), " +
                        "('Clara', 44, 2234.50)");
            }
        } finally {
            if(stmt!=null) stmt.close();
        }
    }

    static void printEmployees() throws SQLException {
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT * FROM Employees");
        // 4. Extract data from result set
        while (rs.next()) {
            int id = rs.getInt("id");
            int age = rs.getInt("age");
            String first = rs.getString("name");
            float salary = rs.getFloat("salary");
            System.out.println("ID: " + id +", First: " + first +", Age: " + age + ", Salary: " + salary);
        }
        rs.close();
    }


    static void insertEmployee(String name, int age, float salary) throws SQLException {
        Statement stmt = conn.createStatement();
        int cnt = stmt.executeUpdate("INSERT INTO Employees (name, age, salary) VALUES ('" + name + "', 75, 10000)",
                Statement.RETURN_GENERATED_KEYS);
        ResultSet rs = stmt.getGeneratedKeys();
        System.out.print("Inserted keys : ");
        while (rs.next()) System.out.print(rs.getString(1) + " ");
        rs.close();
        stmt.close();
    }

    static void insertEmployeeWithPosition(String name, int age, float salary, String position) throws SQLException {
        Statement stmt = conn.createStatement();
        int positionID = -1;

        // 0. Check if such employee exists
        // ...

        // 1. get position id
        String sql = "select id from positions where name = '" + position + "'";
        System.out.println(sql);
        ResultSet rsPos =  stmt.executeQuery("select id from positions where name = '" + position + "'");
        if (rsPos.next()) {  // 2. does position exist?
            positionID = rsPos.getInt("id");
        } else {
            stmt.executeUpdate("INSERT into positions (name) values ('" + position +"')", Statement.RETURN_GENERATED_KEYS);
            ResultSet rsKeys  = stmt.getGeneratedKeys();
            if (rsKeys.next()) {
                positionID = rsKeys.getInt(1);
            }
        }

        // 3. insert new employee
        if (positionID > 0) {
            String sql1 = "INSERT INTO Employees (name, age, salary, fkPosition) VALUES ('" + name + "', '" + age + "', '" + salary + "', '" + positionID + "')";
            System.out.println(sql1);
            int cnt = stmt.executeUpdate(sql1, Statement.RETURN_GENERATED_KEYS);
            ResultSet rs = stmt.getGeneratedKeys();
            System.out.print("Inserted keys : ");
            while (rs.next()) System.out.print(rs.getString(1) + " ");
            rs.close();

        }

        stmt.close();
    }


    // Using ResultSet

    static void insertEmployeeRS(String name, int age, float salary) throws SQLException {
        Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
        String sql = "SELECT * FROM Employees";
        ResultSet rs = stmt.executeQuery(sql);
        rs.moveToInsertRow();
        rs.updateString("name", name);
        rs.updateFloat("salary", salary);
        rs.updateInt("age", age);
        rs.insertRow();
        rs.close();
        stmt.close();
    }

    static void updateEmployeeRS(int n, String name, int age, float salary) throws SQLException {
        Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
        String sql = "SELECT * FROM Employees";
        ResultSet rs = stmt.executeQuery(sql);
        rs.absolute(n);
        rs.updateString("name", name);
        rs.updateInt("age", age);
        rs.updateFloat("salary", salary);
        rs.updateRow();
        rs.close();
        stmt.close();
    }

}

