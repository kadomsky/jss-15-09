package JSS.w02;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class JdbcMetaData {
    // JDBC driver name and database URL
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost/Personnel";
    // Database credentials
    static final String USER = "root";
    static final String PASS = "1234";

    private static Connection conn = null;


    public static void main(String[] args) {
        try {
            // 1. Open a connection
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            // 2. Execute queries
            System.out.println(listTables());

            printTableData("Employees");
            System.out.println();
            printTableData("positions");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }

    private static List<String> listTables() throws SQLException {
        List<String> tables = new ArrayList<>();
        System.out.println("Creating statement...");
        Statement stmt = null;
        try {
            stmt = conn.createStatement();
            DatabaseMetaData metadata = conn.getMetaData();

            // Show all tables
            ResultSet rsTables = metadata.getTables(null, null, null, null);
            while(rsTables.next()) {
                tables.add(rsTables.getString("TABLE_NAME"));
            }

            rsTables.close();

        } finally {
            if(stmt!=null) stmt.close();
        }
        return tables;

    }


    private static void printTableData(String tableName) throws  SQLException {
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT * FROM " + tableName);
        ResultSetMetaData meta  = rs.getMetaData();

        int cnt = meta.getColumnCount();
        for(int i=1; i<=cnt; i++) {
            System.out.print(meta.getColumnName(i) + " (" + meta.getColumnType(i) + ") \t");
        }
        System.out.println();

        while (rs.next()) {
            for (int i=1; i<=cnt; i++) {
                System.out.print(rs.getString(i) + " \t");
            }
            System.out.println();
        }
        rs.close();
        stmt.close();

    }

}
