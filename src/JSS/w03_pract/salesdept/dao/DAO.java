/*
 * Copyright (c) 2020. Cyril Kadomsky.
 */

package JSS.w03_pract.salesdept.dao;

import java.util.List;

public interface DAO<E> {   // Generic interface

    E getById(long id);
    List<E> getAsList();

    long save(E c);
    boolean update(E c);
    boolean delete(E c);
    boolean delete(long id);

}
