/*
 * Copyright (c) 2020. Cyril Kadomsky.
 */

package JSS.w03_pract.salesdept.dao;


import JSS.w03_pract.salesdept.model.Customer;
import JSS.w03_pract.salesdept.model.Order;
import JSS.w03_pract.salesdept.model.Product;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class OrderDao implements DAO<Order> {

    Connection connection;

    public OrderDao(Connection connection) {
        this.connection = connection;
    }

    private Order readOrder(ResultSet rs) throws SQLException {
        Customer customer = new Customer(
                rs.getLong("customer_id"),
                rs.getString("name"),
                rs.getString("Phone"),
                rs.getString("Address"),
                rs.getInt("rating")
        );
        Product product = new Product(
                rs.getLong("product_id"),
                rs.getString("description"),
                rs.getFloat("price")
        );
        return new Order( rs.getLong("order_id"), customer, product,
                rs.getDate("date"), rs.getInt("qty"), rs.getFloat("amount") );

    }

    @Override
    public Order getById(long id)  {
        Order order = null;
        String sql = "select * from orderView where id=?";
        try (PreparedStatement stmt = connection.prepareStatement(sql)) {
            stmt.setLong(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                order = readOrder(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            //todo throw new Exception("Data cannot be retrieved");
        }
        return order;
    }

    @Override
    public List<Order> getAsList() {
        List<Order> orders = new ArrayList<>(100);
        String sql = "select * from orderView";
        try (Statement stmt = connection.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                orders.add( readOrder(rs) );
            }
        } catch (SQLException e) {
            e.printStackTrace();
            //todo throw new Exception("Data cannot be retrieved");
        }
        return orders;
    }

    @Override
    public long save(Order order) {
        order.getCustomer();
        // INSERT into Orders, INSERT into  Customers? Products?
        // long id = ...;
        return -1; // id;
    }

    @Override
    public boolean update(Order order) {
        long id = order.getId();
        // UPDATE .... where id = ?
        int cnt = 0; // stmt.executeUpdate(...);
        return cnt>0;
    }

    @Override
    public boolean delete(Order c) {
        return false;
    }

    @Override
    public boolean delete(long id) {
        return false;
    }
}
