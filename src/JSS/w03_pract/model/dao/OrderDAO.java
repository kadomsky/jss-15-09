package JSS.w03_pract.model.dao;


import JSS.w02_pract.DatabaseUtil;
import JSS.w03_pract.model.om.Customer;
import JSS.w03_pract.model.om.Order;
import JSS.w03_pract.model.om.Product;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class OrderDAO implements DAO<Order> {

    Connection connection = null;
//    LinkedHashMap<Long,Order> buffer = new LinkedHashMap<>(100);

    public OrderDAO(Connection connection) {
        this.connection = connection;
    }

    private Order readOrder(ResultSet rs) throws SQLException {
        Customer customer = new Customer(
                rs.getLong("customer_id"),
                rs.getString("name"),
                rs.getString("Phone"),
                rs.getString("Address"),
                rs.getInt("rating")
        );
        Product product = new Product(
                rs.getLong("product_id"),
                rs.getString("description"),
                rs.getFloat("price")
        );
        return new Order( rs.getLong("order_id"), customer, product,
                rs.getDate("date"), rs.getInt("qty"), rs.getFloat("amount") );

    }

    @Override
    public Order getById(long id) {
        Order order = null;
        String sql = "Select * from orderView where order_id = ?";
        try( PreparedStatement stmt = connection.prepareStatement(sql) ) {
            stmt.setLong(1,id);
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                order = readOrder(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            // todo throw
        }
        return order;
    }

    @Override
    public List<Order> getAsList() {        // todo filter and ordering
        List<Order> orders = new ArrayList<>(100);
        String sql = "Select * from orderView";
        try( Statement stmt = connection.createStatement() ) {
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Order order = readOrder(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return orders;
    }

    @Override
    public long save(Order order) {
        long id = -1;

        String sql = "{call insertOrder(?,?,?,?,?,?)}";
        try ( CallableStatement stmt = connection.prepareCall(sql) ) {

            stmt.setString(1,order.getCustomer().getName());
            stmt.setLong(2, order.getProduct().getId());
            stmt.setDate(3, order.getDate());
            stmt.setInt(4, order.getQty());
            stmt.setFloat(5, order.getAmount());

            stmt.registerOutParameter(6, Types.BIGINT);

            stmt.execute();

            id = stmt.getLong(6);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id;
    }

    @Override
    public boolean update(Order customer) {
        return false;
    }

    @Override
    public boolean delete(Order customer) {
        return false;
    }

    @Override
    public boolean delete(long id) {
        return false;
    }

}
