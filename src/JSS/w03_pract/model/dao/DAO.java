package JSS.w03_pract.model.dao;


import java.util.List;

public interface DAO<T> {

    T getById(long id);

    List<T> getAsList();

    long save(T customer);
    boolean update(T customer);
    boolean delete(T customer);
    boolean delete(long id);

}
