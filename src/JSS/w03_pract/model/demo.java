package JSS.w03_pract.model;


import JSS.w02_pract.DatabaseUtil;
import JSS.w03_pract.model.dao.DAO;
import JSS.w03_pract.model.dao.OrderDAO;
import JSS.w03_pract.model.om.Order;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class demo {

    public static void main(String[] args) {

        try (Connection connection = DatabaseUtil.getMysqlConnection("Personnel")) {

            DAO<Order> orderDAO = new OrderDAO(connection);

            List<Order> list = orderDAO.getAsList();

            for (Order o : list) {
                System.out.println(o.getId() + " \t" + o.getDate() + " \t" + o.getCustomer().getName());
            }

            //Order order = new Order(....);
            //orderDAO.add(order);

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
