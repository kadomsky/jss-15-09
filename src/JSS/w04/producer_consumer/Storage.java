package JSS.w04.producer_consumer;

interface Storage {
    public int getData();
    public void setData(int value);
}
