package JSS.w07_p02;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;

@WebServlet(name = "download", urlPatterns = "/download")
public class DownloadServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doService(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doService(request,response);
    }

    protected void doService(HttpServletRequest request, HttpServletResponse response)  throws ServletException, IOException {
        String fileName = request.getParameter("file") != null ? request.getParameter("file") : "JavaJoy_Student_Manual_v1.2.pdf";
        //serveResourceSimple(fileName,response);
        serveResourceAsUrl(fileName,response);
    }

    protected void serveResourceSimple(String fileName, HttpServletResponse response) throws ServletException, IOException {
        String fullPath = getServletContext().getRealPath("/META-INF/res/data/01.pdf");
        System.out.println("Download : " + fullPath);
        File file = new File(fullPath);

        //File file = new File("d:/IdeaProjects/data/" + fileName); // Needs permissions!!!!

        if (file.exists() && file.isDirectory() == false) {

            response.setContentType(Files.probeContentType(file.toPath()));
            response.addHeader("Content-Disposition", "attachment; filename=" + fileName);
            response.setContentLength((int) file.length());

            System.out.println("Content-type : " + Files.probeContentType(file.toPath()));
            System.out.println("Content-Disposition : attachment; filename=" + fileName);
            System.out.println("Content-length : " + file.length());

//            InputStream fileInputStream = getServletContext().getResourceAsStream("/META-INF/res/data/01.pdf");
//            response.setContentLength(fileInputStream.available());

            FileInputStream fileInputStream = new FileInputStream(file);
            OutputStream responseOutputStream = response.getOutputStream();
            byte buf[] = new byte[1024 * 8];
            int read = 0;
            do {
                read = fileInputStream.read(buf, 0, buf.length);
                responseOutputStream.write(buf, 0, read);
            } while (read > 0);

        } else {
            response.sendError(404,"No such file");
        }
    }

    protected void serveResourceAsUrl(String fileName, HttpServletResponse response) throws ServletException, IOException {

        ServletOutputStream out = response.getOutputStream();
        response.setContentType("text/plain");

        URL url = getServletContext().getResource("/META-INF/res/data/" + fileName);
        if (url==null) {
            out.println("Cannot find resource");
            return;
        }

        URLConnection con = null;
        try {
            con = url.openConnection();
            con.connect();
        } catch (IOException ex) {
            out.println("Cannot read resource");
            ex.printStackTrace();
        }

        response.setContentType(con.getContentType());
        response.addHeader("Content-Disposition", "attachment; filename=" + fileName);
        response.setContentLength(con.getContentLength());

        System.out.println("Content-type : " + con.getContentType());
        System.out.println("Content-Disposition : attachment; filename=" + fileName);
        System.out.println("Content-length : " + con.getContentLength());

        InputStream is = con.getInputStream();
        byte buf[] = new byte[1024 * 8];
        int read = 0;
        do {
            read = is.read(buf, 0, buf.length);
            out.write(buf, 0, read);
        } while (read > 0);

    }

}
