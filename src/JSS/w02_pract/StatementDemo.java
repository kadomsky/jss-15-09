package JSS.w02_pract;

import java.sql.*;

public class StatementDemo {

    public static void main(String[] args) {

        try (Connection connection = DatabaseUtil.getMysqlConnection("Personnel")) {

            usePreparedStatement(connection);
            // useCallableStatement(conn);

        } catch( SQLException e) {
            e.printStackTrace();
        }
    }

    static void usePreparedStatement(Connection connection) throws SQLException {

        PreparedStatement stmt =
                connection.prepareStatement(
                        "select * from employees where name = ? and age = ?");

        stmt.setString(1,"Fred");
        stmt.setInt(2,6);

        ResultSet rs = stmt.executeQuery();

        DatabaseUtil.printResultSet(rs, System.out);

    }

    static void useCallableStatement(Connection connection) throws SQLException {

        CallableStatement cst = connection.prepareCall("{call personnel.getEmployee(?, ?, ?)}");
        cst.setString(1, "Dora");
        cst.registerOutParameter(2, Types.INTEGER);
        cst.registerOutParameter(3, Types.INTEGER);

        cst.execute();

        int id = cst.getInt(2);
        int age = cst.getInt(3);

        System.out.println("id = " + id + ", age = " + age);
    }



}
