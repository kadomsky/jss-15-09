package JSS.w02_pract;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

/**
 * Created by Cyrill on 20.09.2015.
 */
public class TransactionsDemo {

    public static void main(String[] args) {
        try (Connection connection = DatabaseUtil.getMysqlConnection("Personnel")) {
            doTransaction(connection);
            //doTransactionWithException(connection);
            DatabaseUtil.printTableData( "employees", System.out);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    // Must not change the DB
    static void doTransaction(Connection connection) throws SQLException {

        connection.setAutoCommit(false);

        // Transaction 1
        int insertedKey = 0;
        Statement stmt = connection.createStatement();
        stmt.executeUpdate("INSERT INTO Employees (name, age, salary, fkPosition) VALUES ('Tempxxxx', 0, 0, 1) ",
                Statement.RETURN_GENERATED_KEYS);
        ResultSet rsgc = stmt.getGeneratedKeys();
        if (rsgc.next()) {
            insertedKey = rsgc.getInt(1);
        }
        rsgc.close();

        System.out.println("Temp data inserted, id = " + insertedKey + ". Press enter...");
        DatabaseUtil.printTableData("employees", System.out);
        System.out.println("Press enter...");
        Scanner sc = new Scanner(System.in);
        sc.nextLine();

        if (insertedKey!=0) {
            stmt.executeUpdate("DELETE FROM Employees WHERE id=" + insertedKey);
        }
        connection.commit();

        // Transaction 2
        stmt.executeUpdate("DELETE FROM Employees");
        System.out.println("All data deleted, but not commited.");
        DatabaseUtil.printTableData("employees", System.out);
        System.out.println("Press enter...");
        sc.nextLine();
        connection.rollback();

        connection.setAutoCommit(true);
    }

    // Must not change the DB
    static void doTransactionWithException(Connection connection) throws SQLException {
        Statement stmt = null;
        connection.setAutoCommit(false);
        try {
            stmt = connection.createStatement();
            stmt.executeUpdate("INSERT INTO Employees (name, age, salary) VALUES ('Temp', 0, 0) ",
                    Statement.RETURN_GENERATED_KEYS);
            stmt.executeUpdate("INSERT FROM Employees (name, age, salary) VALUES ('Temp', 1, 1) ",   // invalid sql
                    Statement.RETURN_GENERATED_KEYS);
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            connection.rollback();
        } finally {
            if (stmt!=null) {
                stmt.close();
            }
            connection.setAutoCommit(true);
        }
    }

}
