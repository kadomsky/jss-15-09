package JSS.w02_pract;

import java.io.PrintStream;
import java.sql.*;

public final class DatabaseUtil {
    // JDBC database URL
    static final String MYSQL_DB_URL = "jdbc:mysql://localhost/";
    // Database credentials
    static final String MYSQL_USER = "root";
    static final String MYSQL_PASS = "1234";

    private static Connection instance = null;

    public static Connection getMysqlConnection(String dbName) throws SQLException {
        if (instance == null || instance.isClosed()) {
            try {
                // Loading driver class is only necessary for app running in Servlet container.
                Class.forName("com.mysql.cj.jdbc.Driver");  // Loading class `com.mysql.jdbc.Driver'. This is deprecated. The new driver class is `com.mysql.cj.jdbc.Driver'.
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            instance = DriverManager.getConnection(MYSQL_DB_URL + dbName + "?useUnicode=true&characterEncoding=utf8",
                    MYSQL_USER, MYSQL_PASS);
        }
        return instance;
    }

    public static void printTableData(String tableName, PrintStream out) throws  SQLException {
        Statement stmt = instance.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT * FROM " + tableName);
        printResultSet(rs, out);
        rs.close();
        stmt.close();
    }

    public static void printResultSet(ResultSet rs, PrintStream out) throws  SQLException {
        ResultSetMetaData meta  = rs.getMetaData();

        int cnt = meta.getColumnCount();
        for(int i=1; i<=cnt; i++) {
            out.print(meta.getColumnLabel(i) + " (" + meta.getColumnType(i) + ") \t");
        }
        out.println();

        while (rs.next()) {
            for (int i=1; i<=cnt; i++) {
                out.print(rs.getString(i) + " \t");
            }
            out.println();
        }
    }
}


