package JSS.w09_p01.ws.hello;

import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 * @author Cyril Kadomsky
 */

@WebService(serviceName = "HelloService", portName = "HelloPort",
        targetNamespace = "http://ws.JSS.javajoy.net/DemoWS",
        endpointInterface = "JSS.w09_p01.ws.hello.HelloService")
public class HelloServiceImpl implements HelloService {

    public String sayHelloWorldFrom( String from ) {
           String result = "Hello world, from " + from;
           System.out.println(result);
           return result;
    }

    @WebMethod(exclude = true)
    public int getID() {
        return 0;
    }
}
