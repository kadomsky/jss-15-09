package JSS.w09_p01.ws.hello;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

/**
 * @author Cyril Kadomsky
 */

// The implementing class may explicitly reference an SEI through @WebService(endpointInterface="...")
// but is not required to do so. If no endpointInterface is specified in @WebService,
// an SEI is implicitly defined for the implementing class.

// If SEI is used:
// Name, targetNamespace parameters are the name and the namespace of the SEI;
// Implementation class needs to specify SEI in annotation @WebService(endpointInterface = "IMyService")
//   + serviceName, portName  - these optional parameters can only be set in impl class.

// In impl class @WebService annotation parameters only affect the concrete part of WSDL (<wsdl:binding>, <wsdl:service>, <wsdl:port>):
// Annotations in SEI only affect the abstract part of WSDL (<wsdl:types>,<wsdl:message>,<wsdl:portType>).
// If interface is not used, then annotations from impl class are used in both cases.

@WebService(name="HelloService", targetNamespace = "http://JSS.javajoy.net/DemoWS" )
// Style.RPC - веб-сервис будет работать не через сообщения-документы, а как классический RPC, т.е. для вызова метода
// Style.DOCUMENT - обмен сообщения-документы (SOAP)
@SOAPBinding(style= SOAPBinding.Style.DOCUMENT)
public interface HelloService {
    // This annotations override any analogous annotations in implementation class.
    // If annotation is absent, default parameters are set, overriding any parameters in implementation class.
    @WebResult(name="helloString")
    @WebMethod(operationName = "sayHello")
    String sayHelloWorldFrom(@WebParam(name="from", mode= WebParam.Mode.IN)  String from);
}
