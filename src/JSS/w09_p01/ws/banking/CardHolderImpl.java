package JSS.w09_p01.ws.banking;

import JSS.w09_p01.ws.validator.CardValidatorImpl;
import JSS.w09_p01.ws.validator.CreditCard;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.servlet.http.HttpSession;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.handler.MessageContext;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of stateful WS
 * https://docs.oracle.com/cd/E17904_01/web.1111/e13734/stateful.htm#WSADV236
 */

@WebService(name = "CardHolderService", serviceName = "CardHolder",
        targetNamespace = "http://ws.JSS.javajoy.net/banking",
        endpointInterface = "JSS.w09_p01.ws.banking.CardHolder")
public class CardHolderImpl implements CardHolder {
    @Resource    // Step 1
    private WebServiceContext wsContext;    // Step 2

    ArrayList<CreditCard> cards;
    CardValidatorImpl validator;

    public CardHolderImpl() {
        validator = new CardValidatorImpl();
        cards = new ArrayList<>();
    }

    // ArrayList<CreditCard> cards;
    @Override
    public boolean addCreditCard(CreditCard newCard) {
       if (validator.validate(newCard)) {
            //cards.add(newCard);
            addToSession(newCard);
            return true;
       }
       return false;
    }

    public List<CreditCard> getAsList() {
        //return cards;
        return (List<CreditCard>)getSession().getAttribute("cards");
    }

    @WebMethod(exclude = true)
    private HttpSession getSession() {
        // Find the HttpSession
        MessageContext mc = wsContext.getMessageContext();    // Step 3
        HttpSession session = ((javax.servlet.http.HttpServletRequest)mc.get(MessageContext.SERVLET_REQUEST)).getSession();
        if (session == null)
            throw new WebServiceException("No HTTP Session found");
        return session;
        // Get the list object from the HttpSession (or create a new one)
    }

    @WebMethod(exclude = true)
    private void addToSession(CreditCard newCard) {
        HttpSession session = getSession();
        List<CreditCard> cards = (List<CreditCard>)session.getAttribute("cards");  // Step 4
        if (cards == null)
            cards = new ArrayList<CreditCard>(20);
        // Add the item to the list
        cards.add(newCard);
        // Save the updated list in the HTTPSession
        session.setAttribute("cards", cards);
    }
}
