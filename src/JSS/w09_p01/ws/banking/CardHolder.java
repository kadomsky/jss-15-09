package JSS.w09_p01.ws.banking;

import JSS.w09_p01.ws.validator.CreditCard;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

// SEI for stateful WS
@WebService(name = "CardHolder", targetNamespace = "http://ws.JSS.javajoy.net/banking")
public interface CardHolder {

    @WebMethod(operationName = "add")
    @WebResult(name = "success")
    boolean addCreditCard(@WebParam(name = "creditCard", mode= WebParam.Mode.IN) CreditCard newCard);

    @WebMethod(operationName = "list")
    @WebResult(name = "cardsList")
    List<CreditCard> getAsList();

}
