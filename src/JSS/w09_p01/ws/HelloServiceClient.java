package JSS.w09_p01.ws;

import JSS.w09_p01.ws.hello.HelloService;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Cyril on 2020-03-22.
 */
public class HelloServiceClient {

    // Ссылка на wsdl описание сервиса
    public static final String WSDL_LOCATION = "http://localhost:1986/wss/hello?wsdl";
    public static final URL WSDL_LOCATION_URL;

    // Параметры сервиса из WSDL: <wsdl:definitions targetNamespace> и <wsdl:service name>.
    public static final QName SERVICE = new QName("http://ws.JSS.javajoy.net/DemoWS", "HelloService");

    static {
        URL url = null;
        try {
            url = new URL(WSDL_LOCATION);
        } catch (MalformedURLException e) {
            System.out.format("Can not initialize the default wsdl from {0}\n",WSDL_LOCATION);
        }
        WSDL_LOCATION_URL = url;
    }

    public static void main(String[] args) throws MalformedURLException {

        try {
            // Получаем сылку на сервис, описанный в wsdl в теге <service> описании,
            Service service = Service.create(WSDL_LOCATION_URL, SERVICE);
            // Получаем ссылку на нужный порт сервиса (тег port)
            HelloService hello = service.getPort(HelloService.class);
            // Теперь можно вызывать удаленный метод
            System.out.println( hello.sayHelloWorldFrom("XML-RPC") );
        } catch (Exception e) {
            System.out.format("Unable to create service from WSDL %s\n",WSDL_LOCATION);
            e.printStackTrace();
        }
    }

}
