package JSS.w09_p01.ws;

import JSS.w09_p01.ws.banking.CardHolderImpl;
import JSS.w09_p01.ws.hello.HelloServiceImpl;

/**
 * Created by Cyril on 2020-03-22.
 */
public class ServicePublisher {

    public static void main(String... args) {
        javax.xml.ws.Endpoint.publish("http://localhost:1986/wss/hello", new HelloServiceImpl());
    }
}
