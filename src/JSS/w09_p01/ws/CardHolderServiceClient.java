package JSS.w09_p01.ws;

import JSS.w09_p01.ws.banking.CardHolder;
import JSS.w09_p01.ws.validator.CreditCard;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Cyril on 2020-03-22.
 */
public class CardHolderServiceClient {
    // Can access a stateful WS and use HTTP session only if WS is published in container (not by itself),
    // otherwise remote method call fails with SOAPFaultException: No SessionManager

    // Ссылка на wsdl описание сервиса
    public static final String WSDL_LOCATION = "http://localhost:8080/JSS-15-09/services/holder?wsdl";
    public static final URL WSDL_LOCATION_URL;

    // Параметры следующего конструктора смотрим в WSDL: <wsdl:definitions targetNamespace> и <wsdl:service name>.
    public static final QName SERVICE = new QName("http://ws.JSS.javajoy.net/banking", "CardHolder");

    static {
        URL url = null;
        try {
            url = new URL(WSDL_LOCATION);
        } catch (MalformedURLException e) {
            System.out.format("Can not initialize the default wsdl from {0}\n",WSDL_LOCATION);
        }
        WSDL_LOCATION_URL = url;
    }

    public static void main(String[] args) throws MalformedURLException {

        try {
            // Получаем сылку на сервис, описанный в wsdl в теге <service> описании,
            Service service = Service.create(WSDL_LOCATION_URL, SERVICE);
            // Получаем ссылку на нужный порт сервиса (тег port)
            CardHolder cardHolder = service.getPort(CardHolder.class);
            // Enable HTTP session on the client
            ((BindingProvider)cardHolder).getRequestContext().put(BindingProvider.SESSION_MAINTAIN_PROPERTY, true);
            // Теперь можно вызывать удаленный метод
            System.out.println( cardHolder.addCreditCard(new CreditCard("123456",123,"2021-12-01","Mastercard")) );
            System.out.println( cardHolder.addCreditCard(new CreditCard("6543210",123,"2021-12-01","Mastercard")) );
            System.out.println( cardHolder.getAsList() );
        } catch (Exception e) {
            System.out.format("Unable to create service from WSDL %s\n",WSDL_LOCATION);
            e.printStackTrace();
        }
    }

}
