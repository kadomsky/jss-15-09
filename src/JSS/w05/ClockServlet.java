package JSS.w05;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Enumeration;

public class ClockServlet extends javax.servlet.http.HttpServlet {


    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {

        doGet(request,response);
    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
            PrintWriter out = new PrintWriter( response.getWriter() );
            out.println(new Date().toString());

        out.println("Headers:");
        Enumeration<String> headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String h = headerNames.nextElement();
            out.println(h + " = " + request.getHeader(h));
        }

        out.println("Params:");
        Enumeration<String> params = request.getParameterNames();
        while (params.hasMoreElements()) {
            String prm = params.nextElement();
            out.println(prm + " = " + request.getParameter(prm));
        }

    }
}
