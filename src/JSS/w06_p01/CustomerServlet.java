package JSS.w06_p01;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

// Web controller
@WebServlet(name = "CustomerServlet", urlPatterns = "/customers")
public class CustomerServlet extends HttpServlet {
    List<Customer> customers = new ArrayList<>(10);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Use JSP View
        request.setAttribute("customers",customers);  // Map
        getServletContext()
                .getRequestDispatcher("/JSS/w06_p01/customer.jsp")
                .forward(request,response);

    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        customers.add(new Customer("Alice","Kiev, Peremohy 100"));
        customers.add(new Customer("Bob","Kiev, Kherschatyk 25"));
    }
}
