package net.javajoy.jss.w_12_1.orm;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;

/**
 * @author Cyril Kadomsky
 */
@Entity
@Table(name = "orders", schema = "", catalog = "salesdept")
public class Order {
    private long id;
    private Date date;
    private Integer qty;
    private BigDecimal amount;
    private Product product;
    private Customer customer;

    @Id
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Basic
    @Column(name = "qty")
    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    @Basic
    @Column(name = "amount")
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Order order = (Order) o;

        if (id != order.id) return false;
        if (date != null ? !date.equals(order.date) : order.date != null) return false;
        if (qty != null ? !qty.equals(order.qty) : order.qty != null) return false;
        if (amount != null ? !amount.equals(order.amount) : order.amount != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (qty != null ? qty.hashCode() : 0);
        result = 31 * result + (amount != null ? amount.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "product_id", referencedColumnName = "id", nullable = false)
    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @ManyToOne
    @JoinColumn(name = "customer_id", referencedColumnName = "id")
    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", date=" + date +
                ", qty=" + qty +
                ", amount=" + amount +
                ", product=" + product +
                ", customer=" + customer +
                '}';
    }
}
