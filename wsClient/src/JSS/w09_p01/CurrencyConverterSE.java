package JSS.w09_p01;

import JSS.w09_p01.currency.Currency;
import JSS.w09_p01.currency.CurrencyConvertor;
import JSS.w09_p01.currency.CurrencyConvertorSoap;

/**
 * @author Cyril Kadomsky
 * ! Service http://www.webservicex.com/currencyconvertor.asmx is no longer online
 *   Use http://currencyconverter.kowabunga.net/converter.asmx instead
 */
public class CurrencyConverterSE {

    public static void main(String[] args) {
        CurrencyConvertorSoap converter = new CurrencyConvertor().getCurrencyConvertorSoap();
        double rate = converter.conversionRate(Currency.USD, Currency.UAH);
        System.out.println("rate = " + rate);
    }

}
