<%--
  Created by IntelliJ IDEA.
  User: cyril
  Date: 2020-10-06
  Time: 11:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Message received in JSP</title>
</head>
<body>
    <h2>Received message</h2>
    <p> ${message}  <%-- JSP Expression Language --%>
</body>
</html>
