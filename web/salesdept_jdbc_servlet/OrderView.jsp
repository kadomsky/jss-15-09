<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: cyril
  Date: 2020-11-17
  Time: 10:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Orders</title>
</head>
<body>
<H1>Orders</H1>


<c:forEach var="order" items="${requestScope.orders}">
<%-- Layout for a single object --%>
    ${order.date} &nbsp; ${order["qty"]} &nbsp; ${order.customer.name} &nbsp; ${order.product.description}
    &nbsp; <a href=".order/${order.id}">Details</a><br>
<%-- TODO add 'update', 'delete' buttons  --%>
</c:forEach>

<%-- TODO FORM with filters --%>
<%-- TODO add 'Create' button --%>
    <h3> Add new Order </h3>
    <form action="./order/add" method="post">
        Date: <input type="text" name="date">  <br>
        Quantity: <input type="text" name="qty">  <br>
<%--  Unput typew SELECT for Customer, Product  -> use <c:forEach>    --%>
        <input type="submit" value="Create">
    </form>


</body>
</html>
