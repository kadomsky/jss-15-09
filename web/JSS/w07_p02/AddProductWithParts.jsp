<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%!
  public void jspInit() {
    System.out.println("jspInit()");
  }
%>

<html>
<head>
  <title>Upload file</title>
</head>
<body>


<c:set var="message" value=""/>
<c:if test="${requestScope.message != null}">
    <c:set var="message" value="${requestScope.message}"/>
</c:if>

<c:set var="context" value="${pageContext.servletContext.contextPath}"/>

<h1>Add product</h1>

<form method="post" action="${context}/uploadServlet" enctype="multipart/form-data">
  <table border="0">
    <tr>
      <td>Product: </td>
      <td><input type="text" name="description" size="50"/></td>
    </tr>
    <tr>
      <td>Price: </td>
      <td><input type="text" name="price" size="10"/></td>
    </tr>
    <tr>
      <td>Manual file: </td>
      <td><input type="file" name="manual" size="50"/></td>

    </tr>
    <tr>
      <td colspan="2">
        <input type="submit" value="Add">
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <b> ${message} </b>
      </td>
    </tr>
  </table>
</form>
</body>
</html>
