<%@ page import="java.util.List" %>
<%@ page import="JSS.w06_p01.Customer" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<%-- JSP View --%>
<html>
<head>
    <title>Customer list</title>
</head>


<body>
<!--<p>-->
<h2>Customers</h2>
<table>
<%
//    List<Customer> customers = new ArrayList<>(10);   //  To controller
//    customers.add(new Customer("Alice","Kiev, Peremohy 100"));
//    customers.add(new Customer("Bob","Kiev, Kherschatyk 25"));

//    response.setHeader("myBestHeader","value");

    Object contextAttr = application.getAttribute("MyAttrobute");
    String dbURL = application.getInitParameter("dbUrl");
    List<Customer> custemers = (List<Customer>) request.getAttribute("customers");
    for (Customer customer : custemers) {
%>
    <tr><td> <%=customer.getName()%> </td><td> <%=customer.getAddress()%> </td></tr>
<%
    }
%>
</table>

</body>
</html>
