<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Login Success Page</title>
</head>
<body>

    <%
        String userID = "";
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if ( cookie.getName().equals("user_id") ) {
                    userID = cookie.getValue();
                    break;
                }
            }
        }
        String contextPath = application.getContextPath();
        if (!userID.isEmpty()) {
            response.setIntHeader("Expires",0);
            response.setHeader("Pragma", "no-cache");
            response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        } else {
            response.sendRedirect(contextPath+"/JSS/w06_p01/Login.jsp");
        }

    %>

    <h3>Welcome, <%=userID%> </h3><p><p>

    <form action="/logout" method="post">
        <input type="submit" value="Logout" >
    </form>
</body>
</html>