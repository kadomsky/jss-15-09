<%--
  Created by IntelliJ IDEA.
  User: cyril
  Date: 2020-10-20
  Time: 10:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login Page</title>
</head>
<body>

<%
    String contextPath = application.getContextPath();
%>

<form action="<%=contextPath%>/login" method="post">

    Username: <input type="text" name="user">  <br>
    Password: <input type="password" name="pwd">  <br>
    <input type="submit" value="Login">
</form>

</body>
</html>
