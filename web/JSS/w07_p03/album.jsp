<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="jss" uri="http://javajoy.net/JSS/miscTags" %>
<%--
  Created by IntelliJ IDEA.
  User: Cyrill
  Date: 28.11.2015
  Time: 20:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>User's Album</title>
</head>
<body>

    <%--Obtain the reference to ServletContext--%>
    <c:set var="ctx" value="${pageContext.servletContext}"/>

    <%--Create the PhotoDAO instance and store it in session scope--%>
    <jss:photoDAO var="photoDAO"/>

    <h3> Photos: </h3>

    <table>
        <tr>
            <c:forEach var="i" begin="0" end="${photoDAO.size-1}">
                <td>
                    <jss:photo id="${i}"/> <br>
                    ${photoDAO.namesAsList[i]} <br>
                    <a href="/removePhotoServlet?id=${i}"> Remove </a>
                </td>
            </c:forEach>
        </tr>
    </table>
    <p>
    <form action="${ctx.contextPath}/uploadPhoto" method="post" enctype="multipart/form-data">
        <input type="file" name="file" accept="image/*"> <br>
        <input type="submit" value="Upload">
    </form>


</body>
</html>
