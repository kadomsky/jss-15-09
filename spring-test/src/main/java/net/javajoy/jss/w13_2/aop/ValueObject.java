package net.javajoy.jss.w13_2.aop;

/**
 * @author Cyril Kadomsky
 */
public interface ValueObject {

    void setId(long id);
    long getId();

}
