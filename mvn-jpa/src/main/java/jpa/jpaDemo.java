package jpa;

import jpa.model.Customer;
import jpa.model.Order;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class jpaDemo {
    public static void main(String[] args) {
        // Creating Factory is resource-consuming
        EntityManagerFactory emf = Persistence.createEntityManagerFactory( "salesdept-pu" );
        EntityManager em = emf.createEntityManager();


        Customer customer = em.find(Customer.class, 12L);
        System.out.println("Found Customer by id: \n"+ customer);
//        Order order = em.find(Order.class, 1L);
//        System.out.println("Found Order by id: \n"+ order);

//        testJPQL(em);
//        testCriteriaAPI(em);

        em.getEntityManagerFactory().close();
        em.close();
    }

    static void testJPQL(EntityManager em) {
        Query query = em.createQuery("SELECT c from Customer c WHERE c.name like 'J%'");
        Customer singleCust = (Customer) query.getSingleResult();
        System.out.println("Single found Customer: \n" + singleCust);

        List<Customer> list = (List<Customer>)query.getResultList();
        System.out.println("All found customers: ");
        list.forEach(c -> System.out.println(c));
    }

    static void testCriteriaAPI(EntityManager em) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Customer> query = cb.createQuery(Customer.class);     //create query object
        Root<Customer> student = query.from(Customer.class);                //get object representing 'from' part
        query.select(student)                                               // combine 'select' and 'from' parts, equivalent to 'SELECT s FROM Customer c;'
                .where(cb.gt(student.get("rating"), 100));          // where clause

        List<Customer> list = em.createQuery(query).getResultList();
        list.forEach(c->System.out.println(c.getName()));
    }

        static void addCustomer(EntityManager em) {
        em.getTransaction().begin();
        Customer newCustomer = new Customer("Fred","236637462","Some address",35);
        em.persist(newCustomer);
        em.getTransaction().commit();
    }

}
